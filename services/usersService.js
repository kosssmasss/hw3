const { User } = require('../models/Users');
const bcryptjs = require('bcryptjs');

const saveUser = async ({ email, role, password }) => {
  const user = new User({
    email,
    role,
    password: await bcryptjs.hash(password, 10)
  });

  return await user.save();
}

module.exports = {
  saveUser
}
