const fs = require('fs');
const express = require('express');
const morgan = require('morgan');

const app = express();
const mongoose = require('mongoose');

require('dotenv').config();
const file = process.env.CERT_FILE;
mongoose.connect(`mongodb+srv://cluster0.5hugzby.mongodb.net/test?authMechanism=MONGODB-X509&authSource=%24external&tls=true&tlsCertificateKeyFile=${file}`);


// const { filesRouter } = require('./routers/filesRouter.js');
const { trucksRouter } = require('./routers/trucksRouter.js');
const { loadsRouter } = require('./routers/loadsRouter.js');
const { authRouter } = require('./routers/authRouter.js');
const { usersRouter } = require('./routers/usersRouter.js');
const { authMiddleware } = require('./middleware/authMiddleware');
const { isDriver } = require('./middleware/isDriver');

app.use(express.json());
app.use(morgan('tiny'));

// app.use('/api/files', filesRouter);


// app.use('/api/users/me', authMiddleware, usersRouter);
app.use('/api/users/me', authMiddleware, usersRouter);
app.use('/api/auth', authRouter);
app.use('/api/trucks', authMiddleware, isDriver, trucksRouter);
app.use('/api/loads', authMiddleware, loadsRouter);

const start = async () => {
  try {
    app.listen(8080);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

// ERROR HANDLER
app.use(errorHandler);

function errorHandler(err, req, res, next) {
  // res.status(500).send({ message: err.message });
  res.status(500).send({ "message": "string" });
  console.log('err.message', err.message)
}
