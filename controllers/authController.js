const { User, userJoiSchema } = require('../models/Users.js');
const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { saveUser } = require('../services/usersService');

const registerUser = async (req, res, next) => {
  
  const { email, role, password } = req.body;
  await userJoiSchema.validateAsync({email, role, password});

  const user = await saveUser({ email, role, password });
  //return await res.json(user);
  return res.status(200).json({"message": "Profile created successfully"});
    // .catch(err => {
    //   next(err);
    // });
}

// const getUsers = async (req, res, next) => {
//   // const users = await User.aggregate([
//   //   {
//   //     '$limit': 2
//   //   }
//   // ]);

//   const users = await User.aggregate()
//     // .limit(1)
//     .count('users')

//   res.json(users);
// }

const loginUser = async (req, res, next) => {
  const user = await User.findOne({ email: req.body.email });
  console.log('user loginUser', user)
  if (user && await bcryptjs.compare(String(req.body.password), String(user.password))) {
    const payload = { email: user.email, role: user.role, userId: user._id };
    const jwtToken = jwt.sign(payload, 'secret-jwt-key');
    return res.status(200).json({jwt_token: jwtToken});
  }
  return res.status(400).json({"message": "string"});
}

const forgotPassword = async (req, res, next) => {
  const user = await User.findOne({ email: req.body.email });
  if(user){
    return res.status(200).json({"message": "New password sent to your email address"});
  }
  return res.status(400).json({"message": "string"});
}

module.exports = {
  registerUser,
  loginUser,
  // getUsers,
  forgotPassword
};
