const { Truck } = require('../models/Truck.js');

function createBook(req, res, next) {
  const { title, author, userId } = req.body;
  const book = new Book({
    title,
    author,
    userId
  });
  book.save().then((saved) => {
    res.json(saved);
  });
}

function getBooks(req, res, next) {
  return Book.find().then((result) => {
    res.json(result);
  });
}

const getBook = (req, res, next) => Book.findById(req.params.id)
  .then((book) => {
    res.json(book);
  });

const updateBook = async (req, res, next) => {
  const book = await Book.findById(req.params.id);
  const { title, author } = req.body;

  if (title) book.title = title;
  if (author) book.author = author;

  return book.save().then((saved) => res.json(saved));
};

const deleteBook = (req, res, next) => Book.findByIdAndDelete(req.params.id)
  .then((book) => {
    res.json(book);
  });

const getMyBooks = (req, res, next) => {
  return Book.find({userId: req.user.userId}, '-__v').then((result) => {
    res.json(result);
  });
}

const updateMyBookById = (req, res, next) => {
  const { title, author } = req.body;
  return Book.findByIdAndUpdate({_id: req.params.id, userId: req.user.userId}, {$set: { title, author } })
    .then((result) => {
      res.json({message: 'Book was updated'});
    });
}

const markMyBookCompletedById = (req, res, next) => {
  return Book.findByIdAndUpdate({_id: req.params.id, userId: req.user.userId}, {$set: { completed: true } })
    .then((result) => {
      res.json({message: 'Book was marked completed'});
    });
}

// module.exports = {
//   createBook,
//   getBooks,
//   getBook,
//   updateBook,
//   deleteBook,
//   getMyBooks,
//   updateMyBookById,
//   markMyBookCompletedById
// };

let TruckController = {
  getTrucks: async(req,res) =>{
    console.log('getTrucks get', req.user)
    
    if(req.user.role !== 'DRIVER'){
      return res.status(400).json({"message": "string"});
    }
    return Truck.find({'created_by' : req.user.userId}).select("created_by assigned_to type status created_date")
    .then((result) => {
      // console.log('result', result)
      res.status(200).send({
        "trucks": result
        })
      }).catch((err)=>{
        console.log('err', err)
        return res.status(400).json({"message": "string"});
      })
  },
  postTrucks: async(req,res) =>{
    console.log('postTrucks')
    const { type } = req.body;
    console.log('req.user', req.user )
    // if(req.user.role !== 'DRIVER'){
    //   console.log('!DRIVER')
    //   return res.status(400).json({"message": "string"});
    // }
    const truck = new Truck({
      "created_by": req.user.userId,
      "type": req.body.type,
    });
    truck.save().then((saved) => {
          res.status(200).send({
            "message": "Truck created successfully"
        })
    }).catch((err)=>{
      console.log(err)
      return res.status(400).json({"message": "string"});
    });
  },
  getTrucksId: async(req,res) =>{
    // if(req.user.role !== 'DRIVER'){
    //   return res.status(400).json({"message": "string"});
    // }
    Truck.findById(req.params.id).select("created_by assigned_to type status created_date").populate('logs')
    .then((result) => {
      res.status(200).send({
        "truck": result
        })
    }).catch((err)=>{
      console.log(err)
      return res.status(400).json({"message": "string"});
    });
  },
  putTrucksId: async(req,res) =>{
    const { type } = req.body;
    return Truck.findByIdAndUpdate({ _id: req.params.id }, {$set: { type } })
      .then(() => {
        res.json({"message": "Truck details changed successfully"});
      }).catch((err)=>{
        console.log(err)
        return res.status(400).json({"message": "string"});
      });
  },
  deleteTrucksId: async(req,res) =>{
    Truck.findByIdAndDelete(req.params.id)
    .then(() => {
      res.status(200).json({"message": "Truck deleted successfully"});
    }).catch((err)=>{
      console.log(err)
      return res.status(400).json({"message": "string"});
    });
  },
  postTrucksIdAssign: async(req,res) =>{
    return Truck.findByIdAndUpdate({_id: req.params.id }, {$set: { assigned_to: req.user.userId } })
    .then((result) => {
      res.json({"message": "Truck assigned successfully"});
    }).catch((err)=>{
      console.log(err)
      return res.status(400).json({"message": "string"});
    });
  }
}

module.exports = TruckController;