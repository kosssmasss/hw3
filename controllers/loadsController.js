const { Load } = require('../models/Load.js');
const { Log } = require('../models/Log.js');


let LoadController = {
  getLoads: async(req,res) =>{
    let status;
    console.log('req.user.role', req.user.role)
    if(req.user.role === 'DRIVER'){
      status = '$or: [{ status: "NEW" }, { status: "Posted" } ]';
    }

    if(req.user.role === 'SHIPPER'){
      status = '$or: [{ status: "NEW" }, { status: "Posted" } ]';
    }
    
    return Load.find({'created_by' : req.user.userId})
    .select(`
    created_by 
    assigned_to  
    status 
    state 
    name
    payload
    pickup_address
    delivery_address
    dimensions
    logs
    created_date`)
    .then((result) => {
      // console.log('result', result)
      res.status(200).send({
        "loads": result
        })
      }).catch((err)=>{
        console.log('err', err)
        return res.status(400).json({"message": "string"});
      })
  },
  postLoad: async(req,res) =>{
    const { name, payload, pickup_address, delivery_address, dimensions } = req.body;
    const load = new Load({
      "created_by": req.user.userId,
      "name": name,
      "payload": payload,
      "pickup_address": pickup_address,
      "delivery_address": delivery_address,
      "dimensions": dimensions
    });
    load.save().then((saved) => {
      
      const log = new Log({
        "message": "status NEW",
        "owner": req.user.userId,
        "load": load._id
      })
  
      log.save().catch((err)=>{
        console.log('log save err',err)
      })

          res.status(200).send({
            "message": "Load created successfully"
        })
    }).catch((err)=>{
      console.log(err)
      return res.status(400).json({"message": "string"});
    });
  },
  getLoadsActive: async(req,res) =>{
    console.log('getLoadsActive', req.user.role)
    if(req.user.role !== 'DRIVER'){
      return res.status(400).json({"message": "string"});
    }
    Load.find({ 'assigned_to': req.user.id })
    .select("created_by assigned_to status state name payload pickup_address delivery_address dimensions logs created_date")
    .then((result) => {
      res.status(200).send({
        "load": result
        })
    }).catch((err)=>{
      console.log(err)
      return res.status(400).json({"message": "string"});
    });
  },
  patchLoadsActiveState: async(req,res) =>{
    console.log('req.user', req.user)
      if(req.user.role !== 'DRIVER'){
        return res.status(400).json({"message": "string"});
      }
      return Load.findByIdAndUpdate({created_by: req.user.userId }, {$set: { assigned_to: req.user.userId } })
      .then((result) => {
        res.json({ "message": "Load state changed to 'En route to Delivery'" });
      }).catch((err)=>{
        console.log(err)
        return res.status(400).json({"message": "string"});
      });
    },
    postLoadsIdPost: async(req,res) =>{
      if(req.user.role !== 'SHIPPER'){
        return res.status(400).json({"message": "string"});
      }
      return Load.findByIdAndUpdate({ _id: req.params.id }, {$set: { assigned_to: req.user.userId } })
      .then(() => {
        res.json({
          "message": "Load posted successfully",
          "driver_found": true
        });
      }).catch((err)=>{
        console.log(err)
        return res.status(400).json({"message": "string"});
      });
    }
  // putTrucksId: async(req,res) =>{
  //   const { type } = req.body;
  //   return Truck.findByIdAndUpdate({ _id: req.params.id }, {$set: { type } })
  //     .then(() => {
  //       res.json({"message": "Truck details changed successfully"});
  //     }).catch((err)=>{
  //       console.log(err)
  //       return res.status(400).json({"message": "string"});
  //     });
  // },
  // deleteTrucksId: async(req,res) =>{
  //   Truck.findByIdAndDelete(req.params.id)
  //   .then(() => {
  //     res.status(200).json({"message": "Truck deleted successfully"});
  //   }).catch((err)=>{
  //     console.log(err)
  //     return res.status(400).json({"message": "string"});
  //   });
  // },
}

module.exports = LoadController;