const { User, userJoiSchema } = require('../models/Users.js');
const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { saveUser } = require('../services/usersService');

const registerUser = async (req, res, next) => {
  
  const { email, role, password } = req.body;
  await userJoiSchema.validateAsync({email, role, password});

  const user = await saveUser({ email, role, password });
  //return await res.json(user);
  return res.status(200).json({"message": "Profile created successfully"});
    // .catch(err => {
    //   next(err);
    // });
}

const getUser = async (req, res, next) => {
  console.log('getUsergetUser auth req.user', req.user)
  // try{
    const userInfo = await User.findOne({ _id: req.user.userId });
    console.log('userInfo', userInfo)
    if(userInfo === null){
      return res.status(400).json({"message": "string"});
    }
    res.status(200).send({
        "user": {
          "_id": userInfo._id,
          "role": userInfo.role,
          "email": userInfo.email,
          "created_date": userInfo.createdDate
        }
    })
  // }
  // catch(err){
  //   return res.status(500).json({"message": "string"});
  // }
}

const getUsers = async (req, res, next) => {
  // const users = await User.aggregate([
  //   {
  //     '$limit': 2
  //   }
  // ]);

  const users = await User.aggregate()
    // .limit(1)
    .count('users')

  res.json(users);
}

const deleteUser = async (req, res, next) => {
  User.findByIdAndRemove(req.user.userId, (err, doc) => {
    if (!err) {
      res.status(200).send({
        "message": "Profile deleted successfully"
      })
    } else {
      return res.status(400).json({"message": "string"});
    }
  })
}

const patchPassword = async (req, res, next) => {
  const { oldPassword, newPassword } = req.body;
    const user = await User.findOne({ _id: req.user.userId });

    if (user && await bcrypt.compare(String(oldPassword), String(user.password))) {
      user.password = await bcrypt.hash(newPassword, 10)
      user.save()
      return res.json({
        "message": "Password changed successfully"
      })
    }
    return res.json({
      message: "string"
    })
}

module.exports = {
  getUser, 
  deleteUser, 
  patchPassword
};
