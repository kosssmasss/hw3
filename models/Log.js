const mongoose = require("mongoose")
const Schema = mongoose.Schema

let logSchema = new Schema({
  message:{
    type: String,
  },
  time: {
      type: Date,
      default: new Date()
  },
  owner: {
    type: Schema.Types.ObjectId,
    ref: "Loads"
  }
})



const Log = mongoose.model('logs', logSchema);

module.exports = {
  Log,
};
