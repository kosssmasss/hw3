const mongoose = require('mongoose');

const loadsSchema = mongoose.Schema({
    created_by: {
      type: String,
      required: true

    },
    assigned_to: {
      type: String
    },
    status: {
      type: String,
      required: true,
      default: "NEW"
    }, 
    state: {
      type: String,
      required: true,
      default: "En route to Pick Up"
    },
    name: {
      type: String,
      required: true,
      default: "Moving sofa"
    },
    payload: {
      type: Number,
      required: true,
      default: 100
    },
    pickup_address: {
      type: String,
      required: true,
      default: "Flat 25, 12/F, Acacia Building 150 Kennedy Road"
    },
    delivery_address: {
      type: String,
      required: true,
      default: "Sr. Rodrigo Domínguez Av. Bellavista N° 185"
    },
    dimensions: {
      width: {
        type: Number,
        default: 44
      },
      length: { 
        type: Number,
        default: 32
      },
      height: {
        type: Number,
        default: 66
      }
    },
    logs:[
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "load"
      }
    ],
    created_date:{
      type: Date,
      default: new Date()
    }


  // title: {
  //   type: String,
  //   required: true,
  // },
  // author: {
  //   type: String,
  // },
  // userId: {
  //   type: mongoose.Schema.Types.ObjectId,
  //   required: true
  // },
  // completed: {
  //   type: Boolean,
  //   default: false
  // }
});

const Load = mongoose.model('loads', loadsSchema);

module.exports = {
  Load,
};
