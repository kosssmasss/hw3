const mongoose = require('mongoose');

const truckSchema = mongoose.Schema({
    created_by: {
      type: String,
      required: true
    },
    assigned_to: {
      type: String
    },
    type: {
      type: String,
      required: true
    }, 
    status: {
      type: String,
      required: true,
      default: "IS"
    },
    created_date:{
      type: Date,
      default: new Date()
    }


  // title: {
  //   type: String,
  //   required: true,
  // },
  // author: {
  //   type: String,
  // },
  // userId: {
  //   type: mongoose.Schema.Types.ObjectId,
  //   required: true
  // },
  // completed: {
  //   type: Boolean,
  //   default: false
  // }
});

const Truck = mongoose.model('trucks', truckSchema);

module.exports = {
  Truck,
};
