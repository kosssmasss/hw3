const express = require('express');
const router = express.Router();
const { registerUser, loginUser, forgotPassword } = require('../controllers/authController.js');
const { authMiddleware } = require('../middleware/authMiddleware');

const asyncWrapper = (controller) => {
  return (req, res, next) => controller(req, res, next).catch(next);
}

router.post('/register', asyncWrapper(registerUser));

router.post('/login', loginUser);
router.post('/forgot_password', authMiddleware, asyncWrapper(forgotPassword));



module.exports = {
  authRouter: router,
};
