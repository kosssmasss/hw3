const express = require('express');
const router = express.Router();
// const {
//   createBook, getBooks, getBook, deleteBook, updateBook, getMyBooks, updateMyBookById,
//   markMyBookCompletedById
// } = require('../controllers/trucksController.js');

const TruckControlls = require('../controllers/trucksController.js');
// const { authMiddleware } = require('../middleware/authMiddleware');

router.get('/', TruckControlls.getTrucks);
router.post('/', TruckControlls.postTrucks);
router.get('/:id', TruckControlls.getTrucksId);
router.put('/:id', TruckControlls.putTrucksId);
router.delete('/:id', TruckControlls.deleteTrucksId);
router.post('/:id/assign', TruckControlls.postTrucksIdAssign);


module.exports = {
  trucksRouter: router,
};
