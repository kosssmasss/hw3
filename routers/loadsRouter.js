const express = require('express');
const router = express.Router();
// const {
//   createBook, getBooks, getBook, deleteBook, updateBook, getMyBooks, updateMyBookById,
//   markMyBookCompletedById
// } = require('../controllers/trucksController.js');

const LoadsControlls = require('../controllers/loadsController.js');
// const { authMiddleware } = require('../middleware/authMiddleware');

router.get('/', LoadsControlls.getLoads);
router.post('/:id/post', LoadsControlls.postLoadsIdPost);
router.post('/', LoadsControlls.postLoad);
router.get('/active', LoadsControlls.getLoadsActive);
router.patch('/active/state', LoadsControlls.patchLoadsActiveState);
// router.get('/:id', LoadsControlls.getTrucksId);
// router.put('/:id', LoadsControlls.putTrucksId);
// router.delete('/:id', LoadsControlls.deleteTrucksId);
// router.post('/:id/assign', LoadsControlls.postTrucksIdAssign);


module.exports = {
  loadsRouter: router,
};
