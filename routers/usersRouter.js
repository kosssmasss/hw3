const express = require('express');
const router = express.Router();
const { getUser, deleteUser, patchPassword } = require('../controllers/usersController.js');
const { authMiddleware } = require('../middleware/authMiddleware');

const asyncWrapper = (controller) => {
  return (req, res, next) => controller(req, res, next).catch(next);
}

router.get('/', asyncWrapper(getUser));

router.delete('/',  asyncWrapper(deleteUser));
router.patch('/password', asyncWrapper(patchPassword));


module.exports = {
  usersRouter: router,
};
