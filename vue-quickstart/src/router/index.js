import { createWebHistory, createRouter } from "vue-router";

// views
import Home from "../views/Home";
import Login from "../views/Login";
import newTrucks from "../views/newTrucks"

const routes = [
    {
        path: "/",
        name: "Home",
        component: Home,
    },
    {
        path: "/login",
        name: "Login",
        component: Login
    },
    {
      path: "/register",
      name: "Register",
      component: Login
    },
    {
      path: "/newtrucks",
      name: "New Trucks",
      component: newTrucks,

      components: {
        default: () => import("../views/newTrucks"),
        HeaderComponent: () => import("../components/layout/HeaderComponent"),
      }
    }
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;