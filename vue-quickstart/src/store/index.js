import { createStore } from "vuex";
import createPersistedState from 'vuex-persistedstate'

const defaultState = () => {
    return {
        user: null,
        token: null,
        clicks: 0
    }
}

export default createStore({
    plugins: [createPersistedState({
        storage: window.sessionStorage,
    })],
    state: defaultState,
    mutations: {
        loginSuccess(state, token){
          console.log('loginSuccess')
            state.token = token
        },
        setUser(state, obj){
          console.log('setUser', obj)
            state.user = obj.email;
            state.role = obj.role;
            console.log('obj.role', obj.role, 'state.role', state.role)
        },
        addClick() {
            this.state.clicks++;
        },
        resetState(state) {
            Object.assign(state, defaultState())
        }
    },
    getters: {
        user: state => state.user,
        role: state => state.role,
        token: state => state.token,
        clicks: state => state.clicks,
    }
});