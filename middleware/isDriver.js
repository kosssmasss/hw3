const isDriver = (req, res, next) => {
  if(req.user.role !== 'DRIVER'){
    return res.status(400).json({"message": "string NOT DRIVER"});
  }
  next();
}

module.exports = {
  isDriver
}
